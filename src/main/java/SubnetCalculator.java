import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class SubnetCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter an IP address (e.g., 192.168.1.0): ");
        String ipAddress = scanner.nextLine();
        
        System.out.print("Enter a subnet mask (e.g., 255.255.255.0): ");
        String subnetMask = scanner.nextLine();
        
        try {
            InetAddress networkAddress = calculateNetworkAddress(ipAddress, subnetMask);
            InetAddress broadcastAddress = calculateBroadcastAddress(ipAddress, subnetMask);
            int numHosts = calculateNumHosts(subnetMask);
            InetAddress firstUsableHost = calculateFirstUsableHost(networkAddress);
            InetAddress lastUsableHost = calculateLastUsableHost(broadcastAddress);

            System.out.println("Network Address: " + networkAddress.getHostAddress());
            System.out.println("Subnet Mask: " + subnetMask);
            System.out.println("Number of Hosts per Subnet: " + numHosts);
            System.out.println("Usable Host Range: " + firstUsableHost.getHostAddress() + " - " + lastUsableHost.getHostAddress());
            System.out.println("Broadcast Address: " + broadcastAddress.getHostAddress());
        } catch (UnknownHostException e) {
            System.err.println("Invalid IP address or subnet mask format.");
        }
    }

    public static InetAddress calculateNetworkAddress(String ipAddress, String subnetMask) throws UnknownHostException {
        InetAddress ip = InetAddress.getByName(ipAddress);
        InetAddress subnet = InetAddress.getByName(subnetMask);
        byte[] ipBytes = ip.getAddress();
        byte[] subnetBytes = subnet.getAddress();
        byte[] networkBytes = new byte[ipBytes.length];

        for (int i = 0; i < ipBytes.length; i++) {
            networkBytes[i] = (byte)(ipBytes[i] & subnetBytes[i]);
        }

        return InetAddress.getByAddress(networkBytes);
    }

    public static InetAddress calculateBroadcastAddress(String ipAddress, String subnetMask) throws UnknownHostException {
        InetAddress networkAddress = calculateNetworkAddress(ipAddress, subnetMask);
        byte[] networkBytes = networkAddress.getAddress();
        byte[] subnetBytes = InetAddress.getByName(subnetMask).getAddress();
        byte[] broadcastBytes = new byte[networkBytes.length];

        for (int i = 0; i < networkBytes.length; i++) {
            broadcastBytes[i] = (byte)(networkBytes[i] | ~subnetBytes[i]);
        }

        return InetAddress.getByAddress(broadcastBytes);
    }

    public static int calculateNumHosts(String subnetMask) throws UnknownHostException {
        int subnetBits = InetAddress.getByName(subnetMask).getAddress().length * 8;
        return (int) Math.pow(2, 32 - subnetBits) - 2; // Subtract 2 for network and broadcast addresses.
    }

    public static InetAddress calculateFirstUsableHost(InetAddress networkAddress) throws UnknownHostException {
        byte[] networkBytes = networkAddress.getAddress();
        networkBytes[networkBytes.length - 1] += 1; // Increment the last byte to get the first usable host.
        return InetAddress.getByAddress(networkBytes);
    }

    public static InetAddress calculateLastUsableHost(InetAddress broadcastAddress) throws UnknownHostException {
        byte[] broadcastBytes = broadcastAddress.getAddress();
        broadcastBytes[broadcastBytes.length - 1] -= 1; // Decrement the last byte to get the last usable host.
        return InetAddress.getByAddress(broadcastBytes);
    }
}

